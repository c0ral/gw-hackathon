import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import time






cred = credentials.Certificate("serviceAccountKey.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

users_ref = db.collection(u'users')
#users = users_ref.get()
locations_ref = db.collection(u'weigh_ready')


location_dict = {}
user_dict = {}
for location in locations_ref.get():
	location_dict[location.id] = location.get(u'uid')
for user in users_ref.get():
	user_dict[user.id] = user

def update_info(location, uid):	
	user = user_dict[uid]
	users_ref.document(uid).set({u'plastic':user.get(u'plastic') + location.get(u'pscale') - location.get(u'plastic'),
			  u'glass':user.get(u'glass') + location.get(u'gscale') - location.get(u'glass'),
			  u'metal':user.get(u'metal') + location.get(u'mscale') - location.get(u'metal'),
			  u'compost':user.get(u'compost') + location.get(u'cscale') - location.get(u'compost')
			  }, merge=True)
	locations_ref.document(location.id).set({u'plastic':location.get(u'pscale'),
											 u'glass':location.get(u'gscale'),
											 u'metal':location.get(u'mscale'),
											 u'compost':location.get(u'cscale')}, merge=True)
def get_input(location):
	user_input = raw_input("please enter the weight of the the following items: plastic, glass, metal, and compost separated by commas\n")
	user_input.replace(" ","")
	weights = user_input.split(",")
	locations_ref.document(location.id).set({u'pscale':int(weights[0]),
											 u'gscale':int(weights[1]),
											 u'mscale':int(weights[2]),
											 u'cscale':int(weights[3])}, merge=True)
now = time.time() % 60
in_use = set()


while (True):
	if (time.time() % 60 != now):
		now = time.time() % 60
		for location in locations_ref.get():
			uid = location_dict[location.id]
			if (uid != location.get(u'uid')):
				if (uid == ''):
					location_dict[location.id] = location.get(u'uid')
					get_input(location)
				else:
					update_info(location, uid)
					location_dict[location.id] = ''
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgRankingComponent } from './org-ranking.component';

describe('OrgRankingComponent', () => {
  let component: OrgRankingComponent;
  let fixture: ComponentFixture<OrgRankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

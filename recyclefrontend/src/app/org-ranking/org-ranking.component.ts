import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Component({
  selector: 'app-org-ranking',
  templateUrl: './org-ranking.component.html',
  styleUrls: ['./org-ranking.component.scss']
})
export class OrgRankingComponent implements OnInit {

  constructor(private afs: AngularFirestore) { }

  public orgsList = [];
  public scores = {};

  // TODO remove duplication from org-detail
  private scoreUser(user): number {
    return user.compost + user.glass + user.metal + user.plastic;
  }

  private sortOrgs() {
      this.orgsList.sort((a, b) => this.scores[b] - this.scores[a]);
  }

  ngOnInit() {
    this.afs.collection('orgs').snapshotChanges().subscribe(orgs => {
      orgs.map(item => item.payload.doc.id).forEach(name => this.orgsList.push(name));
    });

    this.afs.collection('users').valueChanges().subscribe(() => {
      for (let orgName of this.orgsList) {
        this.afs.collection('users', ref => ref.where('org', '==', orgName)).valueChanges()
          .subscribe(members => {
            let score = members.reduce((a: number, b) => a + this.scoreUser(b), 0)
            this.scores[orgName] = score;
            this.sortOrgs();
          });
      }
      this.sortOrgs();
    });
  }

}

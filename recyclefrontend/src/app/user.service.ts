import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public name: String;
  public uid: String;
  public recycleLocation: String;
  public org: String;
  constructor(private afs: AngularFirestore) {
    this.recycleLocation = localStorage.getItem('location');
    this.uid = localStorage.getItem('uid');
    this.afs.doc('users/' + this.uid).valueChanges().subscribe((user: any) => {
      console.log(user);
      this.org = user.org;
      this.name = user.name;
    });
  }
}

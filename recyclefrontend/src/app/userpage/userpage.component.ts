import { Component, OnInit } from '@angular/core';
import { ProfilegraphComponent } from '../profilegraph/profilegraph.component';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    selector: 'app-userpage',
    templateUrl: './userpage.component.html',
    styleUrls: ['./userpage.component.scss']
})
export class UserpageComponent implements OnInit {

    constructor(private afs: AngularFirestore, private route: ActivatedRoute, private location: Location) { }

    public orgList: string;
    public userID: any;
    public userInfo: any;

    ngOnInit() {
        this.userID = this.route.snapshot.paramMap.get('id');
        this.userInfo = this.afs.doc('users/' + this.userID).valueChanges();
    }
}

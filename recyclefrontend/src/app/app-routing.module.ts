import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { AuthComponent } from './auth/auth.component';
import { OrgDetailComponent } from './org-detail/org-detail.component';
import { UserpageComponent } from './userpage/userpage.component';
import { OrgRankingComponent } from './org-ranking/org-ranking.component'

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: AuthComponent },
  { path: 'org/:name', component: OrgDetailComponent },
  { path: 'users/:id', component: UserpageComponent },
  { path: 'rankings', component: OrgRankingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

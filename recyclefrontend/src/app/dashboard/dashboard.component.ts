import { Component, OnInit } from '@angular/core';
import { ProfilegraphComponent } from '../profilegraph/profilegraph.component';
import { UserService } from '../user.service';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public userService: UserService, private afs: AngularFirestore) { }

  public userInfo: any;
  public isWeighing: boolean = false;

  ngOnInit() {
    this.userInfo = this.afs.doc('users/' + this.userService.uid).valueChanges();
  }


  beginWeigh() {
    this.isWeighing = true;
    this.afs.doc('weigh_ready/' + this.userService.recycleLocation).update({uid: this.userService.uid})
  }

  endWeigh() {
    this.isWeighing = false;
    this.afs.doc('weigh_ready/' + this.userService.recycleLocation).update({uid: ''})
  }

}

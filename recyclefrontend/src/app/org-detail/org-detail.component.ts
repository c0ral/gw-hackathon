import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { UserService } from '../user.service';


@Component({
  selector: 'app-org-detail',
  templateUrl: './org-detail.component.html',
  styleUrls: ['./org-detail.component.scss']
})
export class OrgDetailComponent implements OnInit {

  public recycleData;
  public dataStream = new Subject();
  public orgName: string;
  public leaderboard;

  constructor(public userService: UserService, private afs: AngularFirestore, private route: ActivatedRoute, private location: Location) { }

  private scoreUser(user): number {
    return user.compost + user.glass + user.metal + user.plastic;
  }

  ngOnInit() {
    this.orgName = this.route.snapshot.paramMap.get('name');
    this.recycleData = this.afs.collection('users', ref => ref.where('org', '==', this.orgName)).snapshotChanges().pipe(
      map(users => users.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      })));

    this.recycleData.subscribe(members => {
      console.log(members.payload);
      members = members.sort((a, b) => this.scoreUser(b) - this.scoreUser(a))
      let sums = {}
      let keys = ['compost', 'glass', 'metal', 'plastic']
      for (let member of members) {
        for (let key of keys) {
          if (!(key in sums)) sums[key] = 0;
          sums[key] += member[key];
        }
      }
      this.dataStream.next(sums);
      this.leaderboard = members;
    });
  }
}

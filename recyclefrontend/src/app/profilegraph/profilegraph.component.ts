import { Component, OnInit, Input } from '@angular/core';
import { Chart, ChartType, ChartOptions } from 'chart.js';
import { Color, MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-profilegraph',
  templateUrl: './profilegraph.component.html',
  styleUrls: ['./profilegraph.component.scss']
})
export class ProfilegraphComponent implements OnInit {

  @Input() data: any;

  public doughnutChartLabels: Label[] = ['Glass', 'Aluminum', 'Plastic', 'Compost'];
  public doughnutChartData: MultiDataSet = [
    [0, 0, 0, 0],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public options: ChartOptions = { 
    responsive: true,
    maintainAspectRatio: false,
    layout: {
      padding: 0,
    },
    legend: { 
      position: 'bottom',
      labels: {
        boxWidth: 20,
        fontSize: 14,
      }
    }
  };

  constructor() { }

  ngOnInit() {
    this.data.subscribe(o => {
      this.doughnutChartData = [[o.glass, o.metal, o.plastic, o.compost]]
    });
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalweightComponent } from './totalweight.component';

describe('TotalweightComponent', () => {
  let component: TotalweightComponent;
  let fixture: ComponentFixture<TotalweightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalweightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalweightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

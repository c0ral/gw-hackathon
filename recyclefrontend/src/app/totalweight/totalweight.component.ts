import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-totalweight',
    templateUrl: './totalweight.component.html',
    styleUrls: ['./totalweight.component.scss']
})
export class TotalweightComponent implements OnInit {

    @Input() data: any;

    public totalWeight: 0;

    constructor() { }

    ngOnInit() {
        this.data.subscribe(o => {
            this.totalWeight = o.glass + o.metal + o.plastic + o.compost;
        });
    }

}

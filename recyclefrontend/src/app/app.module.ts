import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { ProfilegraphComponent } from './profilegraph/profilegraph.component';
import { AuthComponent } from './auth/auth.component';
import { UserService } from './user.service';
import { OrgDetailComponent } from './org-detail/org-detail.component';
import { UserpageComponent } from './userpage/userpage.component';
import { OrgRankingComponent } from './org-ranking/org-ranking.component';
import { TotalweightComponent } from './totalweight/totalweight.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfilegraphComponent,
    AuthComponent,
    OrgDetailComponent,
    UserpageComponent,
    OrgRankingComponent,
    TotalweightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
